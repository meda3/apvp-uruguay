Estimación de los Años Potenciales de Vida Perdidos y su incertidumbre para las muertes por isquemia cardíaca para Uruguay en 2018

El primer objetivo de este proyecto es demostrar la utilidad del indicador Años Potenciales de Vida Perdidos (APVP) para la evaluación del nivel/impacto de la mortalidad y el desarrollo de políticas sanitarias. El segundo objetivo es contribuir a la incorporación de la perspectiva probabilística al tratamiento de la información demográfica. 

Los APVP proveen una perspectiva diferente y complementaria a otros indicadores del nivel de mortalidad más frecuentemente utilizados, como la esperanza de vida, al permitir evaluar el impacto ocasionado por las muertes prematuras asociadas a distintas causas. 

Se plantean difderentes alternativas de cálculo de los APVP que Arriaga (1996) describen como la suma algebraica de los años de vida que potencialmente hubiesen vivido los individuos que fallecen por una cierta causa considerando una cierta edad límite $L$ de supervivencia.
Se proponen 3 variantes para fijar el límte L. Por un lado se fija L como un valor fijo, consideramdo también ese límite como la esperanza de vida al nacer y consignado como $L_0$ y finalmente una tercer variante del límite establecido como la esoeranza de vida a la edad X, consignado como $L_x$

El enfoque tradicional utilizado en Demografía consiste en considerar el número de muertes por determinada causa en un determinado período de tiempo como el reflejo de un fenómeno determinístico.  El enfoque propuesto en ese trabajo consiste en entender estas cantidades como la realización de un proceso estocástico, es decir entender la mortalidad como un proceso con un componente aleatorio. Esto implica tratar los APVP como una variable aleatoria, que debe ser caracterizada también por su incertidumbre. Para eso se propone como primera alternativa estimar la varianza de los APVP por isquemia cardíaca en Uruguay en 2018 y recoger la variabilidad en el muestreo mediante simulación Monte Carlo, usando la técnica Bootstrap. Se plantea también como complemento a la evaluación puntual del APVP y su varianza, considerar el cambio de ese índice condicionado a la edad x, denominado $APVP_x$, para el que se construye una banda de confianza en base  a los cuantiles empíricos.

En el directorio "funciones" se encuentra el código de R necesario para calcular los indicadores y obtener la salida gráfica.

En el directorio "Shiny" se encuentra el código de R necesario para la creacion de la aplicacion web. La siguiente se puede acceder a traves
del siguiente link : https://alvaro3432.shinyapps.io/APVP/
   